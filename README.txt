Final Project:  Checkers
By: Joshua Ertl
CS 459-401

-	Operational checkers game
-	Mix of opaque and transparent layers
-	Animation
-	3D lighting
-	Simple shadows
-	Interactive menu


Note:  It seems the source code in "main.cpp" does not successfully execute under Visual Studio 2010,
however it does in CodeBlocks which was also available to us.  By installing (or using the binary package)
CodeBlocks with MinGW and replacing references of "Visual Studio" with "MinGW" within the CodeBlocks download in the lab
supplied installation instructions for OpenGL a project for GLUT can be made.  By choosing a new GLUT project in CodeBlocks,
first use after installation will require locating the OpenGL components which can be completed by skipping the first screen
where it is asked that contains variable information and manually selecting the same "MinGW" folder in the folder browser.

The executable used in the class demo is supplied, generated using CodeBlocks, we just renamed it.


Controls:
-	WASD or Arrow Keys:  Move selector
-	Enter or Spacebar:  Confirm action (select, move)
-	Escape:  Deselect piece

-	Right click:  Display Customizer Menu
-	V:  Toggle 3D lighting
-	- or _ (dash, minus or underscore):  Toggle light flicker effect
-	= or + (equal or plus):  Toggle shadow flicker effect
-	\ or | (backslash or vertical):  Toggle idle updating animation
-	, or < (comma or less than):  Decrement customizer value
-	. or > (period or greater than):  Increment customizer value