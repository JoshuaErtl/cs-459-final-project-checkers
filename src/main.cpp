/////////////////////////////////////////
//      Final Project:  Checkers       //
//           By: Joshua Ertl           //
/////////////////////////////////////////

#include <windows.h>

#include <GL/glut.h>
#include <math.h>

#define PI 3.14159265

////////////////////////  OPTIONS  /////////////////////////
static float CAMERA_DISTANCE = 40.0;
static float CAMERA_HEIGHT = 50.0;

static float PIECE_COLOR[2][3] = {
	{0.25, 0.25, 0.25},
	{1.0, 0.2, 0.2}
};
static float SQUARE_COLOR[2][3] = {
	{0.4, 0.2, 0.2},
	{0.85, 0.7, 0.5}
};
static float EDGE_COLOR[3] = {0.4, 0.1, 0.08};

static float DARKEN_OFFSET = 0.15;

static float SELECTOR_COLOR[3][4] = {
	{0.8, 1.0, 0.8, 0.8},
	{0.2, 1.0, 0.2, 0.8},
	{1.0, 0.2, 0.2, 0.8}
};
static float POSSIBLE_TARGET_COLOR[4] = {1.0, 1.0, 0.5, 0.8};
static float TARGET_COLOR[4] = {1.0, 0.6, 0.2, 0.8};

static float LIGHT_COLOR[4] = {1.0, 0.6, 0.2, 1.0};

static float SHADOW_COLOR_INTENSITY = 0.0;
static float SHADOW_COLOR_ALPHA = 0.6;

static float DEFAULT_COLOR[4] = {0.0, 0.0, 0.0, 1.0};

static int BOARD_DIMENSION = 8;
static int STARTING_ROWS = 3;

static float SQUARE_SIZE = 5.0;
static float EDGE_HEIGHT = 0.5;
static float EDGE_THICKNESS = 2.0;
static float PIECE_HEIGHT = 1.0;
static float PIECE_RADIUS = 2.0;
static int PIECE_EDGES = 16;

static float SELECTOR_HEIGHT_OFFSET = 0.002;

static int ANIMATION_DELAY = 10;
static float ANIMATION_INCREMENT = 0.01;
static float MOVING_ANIMATION_SPEEDUP = 3.0;
static int FLICKER_RANGE = 25;
static float FLICKER_MULTIPLIER = 0.005;

static float MOVING_PIECE_HEIGHT = 5.0;

static float LIGHT_POSITION[4] = {0.0, 35.0, 25.0, 1.0};

static float SHADOW_HEIGHT_OFFSET = 0.001;
static float SHADOW_SIZE_MULTIPLIER = 1.1;
////////////////////////////////////////////////////////////

enum MENU_ENTRY{
	// Camera Menu
	MENU_CAMERA_DISTANCE,				// float CAMERA_DISTANCE
	MENU_CAMERA_HEIGHT,					// float CAMERA_HEIGHT
	// Color Menu
		// Pieces Menu
			// Player 1 Menu
	MENU_COLOR_PIECE_1_R,				// float PIECE_COLOR[0][0]
	MENU_COLOR_PIECE_1_G,				// float PIECE_COLOR[0][1]
	MENU_COLOR_PIECE_1_B,				// float PIECE_COLOR[0][2]
			// Player 2 Menu
	MENU_COLOR_PIECE_2_R,				// float PIECE_COLOR[1][0]
	MENU_COLOR_PIECE_2_G,				// float PIECE_COLOR[1][1]
	MENU_COLOR_PIECE_2_B,				// float PIECE_COLOR[1][2]
		// Squares Menu
			// Left Menu
	MENU_COLOR_SQUARE_1_R,				// float SQUARE_COLOR[0][0]
	MENU_COLOR_SQUARE_1_G,				// float SQUARE_COLOR[0][1]
	MENU_COLOR_SQUARE_1_B,				// float SQUARE_COLOR[0][2]
			// Right Menu
	MENU_COLOR_SQUARE_2_R,				// float SQUARE_COLOR[1][0]
	MENU_COLOR_SQUARE_2_G,				// float SQUARE_COLOR[1][1]
	MENU_COLOR_SQUARE_2_B,				// float SQUARE_COLOR[1][2]
		// Edge Menu
	MENU_COLOR_EDGE_R,					// float EDGE_COLOR[0]
	MENU_COLOR_EDGE_G,					// float EDGE_COLOR[1]
	MENU_COLOR_EDGE_B,					// float EDGE_COLOR[2]
		// Selector Menu
			// Normal Menu
	MENU_COLOR_SELECTOR_1_R,			// float SELECTOR_COLOR[0][0]
	MENU_COLOR_SELECTOR_1_G,			// float SELECTOR_COLOR[0][1]
	MENU_COLOR_SELECTOR_1_B,			// float SELECTOR_COLOR[0][2]
	MENU_COLOR_SELECTOR_1_A,			// float SELECTOR_COLOR[0][3]
			// Valid Menu
	MENU_COLOR_SELECTOR_2_R,			// float SELECTOR_COLOR[1][0]
	MENU_COLOR_SELECTOR_2_G,			// float SELECTOR_COLOR[1][1]
	MENU_COLOR_SELECTOR_2_B,			// float SELECTOR_COLOR[1][2]
	MENU_COLOR_SELECTOR_2_A,			// float SELECTOR_COLOR[1][3]
			// Invalid Menu
	MENU_COLOR_SELECTOR_3_R,			// float SELECTOR_COLOR[2][0]
	MENU_COLOR_SELECTOR_3_G,			// float SELECTOR_COLOR[2][1]
	MENU_COLOR_SELECTOR_3_B,			// float SELECTOR_COLOR[2][2]
	MENU_COLOR_SELECTOR_3_A,			// float SELECTOR_COLOR[2][3]
		// Possible Target Menu
	MENU_COLOR_POSSIBLE_TARGET_R,		// float POSSIBLE_TARGET_COLOR[0]
	MENU_COLOR_POSSIBLE_TARGET_G,		// float POSSIBLE_TARGET_COLOR[1]
	MENU_COLOR_POSSIBLE_TARGET_B,		// float POSSIBLE_TARGET_COLOR[2]
	MENU_COLOR_POSSIBLE_TARGET_A,		// float POSSIBLE_TARGET_COLOR[3]
		// Target Menu
	MENU_COLOR_TARGET_R,				// float TARGET_COLOR[0]
	MENU_COLOR_TARGET_G,				// float TARGET_COLOR[1]
	MENU_COLOR_TARGET_B,				// float TARGET_COLOR[2]
	MENU_COLOR_TARGET_A,				// float TARGET_COLOR[3]
		// Light Menu
	MENU_COLOR_LIGHT_R,					// float LIGHT_COLOR[0]
	MENU_COLOR_LIGHT_G,					// float LIGHT_COLOR[1]
	MENU_COLOR_LIGHT_B,					// float LIGHT_COLOR[2]
		// Shadow Menu
	MENU_COLOR_SHADOW_I,				// float SHADOW_COLOR_INTENSITY
	MENU_COLOR_SHADOW_A,				// float SHADOW_COLOR_ALPHA
		// Side Darken Entry
	MENU_COLOR_DARKEN,					// float DARKEN_OFFSET
	// Gameplay Menu
	MENU_GAME_BOARD_DIMENSIONS,			// int BOARD_DIMENSION
	MENU_GAME_STARTING_ROWS,			// int STARTING_ROWS
	// Visual Menu
		// Piece Menu
	MENU_VISUAL_PIECE_EDGES,			// int PIECE_EDGES
	MENU_VISUAL_PIECE_HEIGHT,			// float PIECE_HEIGHT
	MENU_VISUAL_PIECE_RADIUS,			// float PIECE_RADIUS
		// Edge Menu
	MENU_VISUAL_EDGE_HEIGHT,			// float EDGE_HEIGHT
	MENU_VISUAL_EDGE_THICKNESS,			// float EDGE_THICKNESS
		// Square Size Entry
	MENU_VISUAL_SQUARE_SIZE,			// float SQUARE_SIZE
		// Selector Height Entry
	MENU_VISUAL_SELECTOR_HEIGHT,		// float SELECTOR_HEIGHT_OFFSET
		// Light Position Menu
	MENU_VISUAL_LIGHT_POSITION_X,		// float LIGHT_POSITION[0]
	MENU_VISUAL_LIGHT_POSITION_Y,		// float LIGHT_POSITION[1]
	MENU_VISUAL_LIGHT_POSITION_Z,		// float LIGHT_POSITION[2]
		// Shadow Menu
	MENU_VISUAL_SHADOW_HEIGHT,			// float SHADOW_HEIGHT_OFFSET
	MENU_VISUAL_SHADOW_MULTIPLIER,		// float SHADOW_SIZE_MULTIPLIER
	// Animation Menu
		// Flicker Menu
	MENU_ANIMATION_FLICKER_RANGE,		// int FLICKER_RANGE
	MENU_ANIMATION_FLICKER_MULTIPLIER,	// float FLICKER_MULTIPLIER
		// Entries
	MENU_ANIMATION_DELAY,				// int ANIMATION_DELAY
	MENU_ANIMATION_INCREMENT,			// float ANIMATION_INCREMENT
	MENU_ANIMATION_MOVING_SPEEDUP,		// float MOVING_ANIMATION_SPEEDUP
	MENU_ANIMATION_MOVING_HEIGHT,		// float MOVING_PIECE_HEIGHT
};

struct VECTOR{
	float v[3];
	VECTOR(float v1, float v2, float v3){
		v[0] = v1;
		v[1] = v2;
		v[2] = v3;
	}
	VECTOR(){};
};


// HEADERS //
void calculateEdgeLengths();
void calculatePieceVerticies();
void calculateSideColors();

float* getNormal();
bool isActivePlayerPiece();
bool isInBoard();

void setAnimation();
void postAnimation();

void animateIdle();
void animateKingPiece();
void animateMovePiece();
void animateRemovePiece();
void animateRotateCamera();

void postAnimateKingPiece();
void postAnimateMovePiece();
void postAnimateRemovePiece();
void postAnimateRotateCamera();

void decrementCustomizer();
void incrementCustomizer();

void collectTargets();
bool decrementSelector();
void finishMove();
bool incrementSelector();
void moveSelector();
void movePiece();
bool removePiece();
void resetBoard();
void toggleActivePlayer();
void toggleSpecialShading();
////////////

static VECTOR VECTOR_EAST = VECTOR(1.0, 0.0, 0.0);
static VECTOR VECTOR_NORTH = VECTOR(0.0, 0.0, 1.0);
static VECTOR VECTOR_SOUTH = VECTOR(0.0, 0.0, -1.0);
static VECTOR VECTOR_UP = VECTOR(0.0, 1.0, 0.0);
static VECTOR VECTOR_WEST = VECTOR(-1.0, 0.0, 0.0);

static float EDGE_INNER_LENGTH;
static float EDGE_JUMP_LENGTH;
static float EDGE_OUTER_LENGTH;
static float EDGE_OUTER_HEIGHT;

static float* PIECE_VERTICIES[2];  //  {{X, Z}, ...}

static float PIECE_SIDE_COLORS[2][3];
static float EDGE_SIDE_COLOR[3];


static int** gameBoard;  //  gameBoard[Y][X] = (value > 1 = is a piece, value bit 1 = is player 1, value bit 2 = is player 2, value bit 3 = is kinged)

static int*** moveToRemoves;  //  moveToRemoves[Y][X] = {y, x} of piece to be removed if move goes to X, Y.  x, y = -1 if no piece is to be removed
static bool** validTargets;  //  validTargets[Y][X] = true if square can be moved to

static int selectX, selectY;  //  Selector position

static bool canDeselect;  //  True if player can deselect the current moving piece
static bool continuingMove;  //  True if player is still able to claim enemy pieces
static bool hasRemovableTargets;  //  True if enemy pieces are targeted
static bool hasTargets;  //  True if any squares are targeted
static bool isSelectingTarget;  //  True if player has selected piece and is now choosing destination target

static bool isKingingPiece = false;  //  True if a piece is being kinged
static bool isMovingPiece = false;  //  True if a piece should be moving
static int movingPiecePos[2];  //  {Y, X} position of moving piece on gameBoard

static float animationTime;  //  0.0 - 1.0 animation progression
static float cameraAngle;
static float movingX, movingY, movingZ;  //  X, Y, Z additional movement for moving pieces

static bool canInteract = false;  //  Locks player from moving and drawing special tiles
static bool isPlayerOne;  //  True if first player is active

static bool doIdleFunction = true;
static bool lightFlicker = true;
static bool shadowFlicker = true;
static bool specialShading = true;  //  True if special drawing should be done


// DEPENDENT CALCULATIONS //
void calculateEdgeLengths(){
	EDGE_INNER_LENGTH = SQUARE_SIZE * BOARD_DIMENSION;
	EDGE_JUMP_LENGTH = EDGE_INNER_LENGTH + EDGE_THICKNESS;
	EDGE_OUTER_HEIGHT = EDGE_HEIGHT * 2;
	EDGE_OUTER_LENGTH = EDGE_JUMP_LENGTH + EDGE_THICKNESS;
}

void calculatePieceVerticies(){
	for(int i = 0; i < 2; i++){
		if(PIECE_VERTICIES[i]){
			delete[] PIECE_VERTICIES[i];
		}
		PIECE_VERTICIES[i] = new float[PIECE_EDGES];
	}
	float angleChange = (2.0 * PI) / (float) PIECE_EDGES;
	float angle = 0;
	for(int i = 0; i < PIECE_EDGES; i++){
		PIECE_VERTICIES[0][i] = cos(angle) * PIECE_RADIUS;
		PIECE_VERTICIES[1][i] = sin(angle) * PIECE_RADIUS;
		angle += angleChange;
	}
}

void calculateSideColors(){
	for(int i = 0; i < 3; i++){
		EDGE_SIDE_COLOR[i] = EDGE_COLOR[i] - DARKEN_OFFSET;
		for(int j = 0; j < 2; j++){
			PIECE_SIDE_COLORS[j][i] = PIECE_COLOR[j][i] - DARKEN_OFFSET;
		}
	}
}
////////////////////////////


// ACTIVITY CHECKS //
// Calculates normal from three vertices
float normalValues[3];
float* getNormal(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3){
	float n1x = x2 - x1, n1y = y2 - y1, n1z = z2 - z1, n2x = x3 - x1, n2y = y3 - y1, n2z = z3 - z1;

	float normX = (n1y * n2z) - (n2y * n1z), normY = (n1z * n2x) - (n2z * n1x), normZ = (n1x * n2y) - (n2x * n1y);

	float factor = sqrt(pow(normX,2) + pow(normY,2) + pow(normZ,2));
	normX /= factor;
	normY /= factor;
	normZ /= factor;

	normalValues[0] = normX;
	normalValues[1] = normY;
	normalValues[2] = normZ;
	return normalValues;
}

// True if a game board piece is owned by current player
bool isActivePlayerPiece(int piece){
	return (piece & 0x3) == (isPlayerOne ? 1 : 2);
}

// True if the position is within the board dimensions
bool isInBoard(int y, int x){
	return y < BOARD_DIMENSION && y >= 0 && x < BOARD_DIMENSION && x >= 0;
}
/////////////////////


// PLAY ANIMATIONS //
// Sets idle function to animation 'func'
void setAnimation(void (*func)(void)){
	canInteract = false;
	animationTime = 0.0;
	glutIdleFunc(func);
}

// Animation cleanup
void postAnimation(void (*func)(void)){
	glutPostRedisplay();
	if(animationTime >= 1.0){
		glutIdleFunc(doIdleFunction ? animateIdle : NULL);
		canInteract = true;
		if(func){
			func();
		}
	}
	else{
		Sleep(ANIMATION_DELAY);
	}
}
/////////////////////


// AVAILABLE ANIMATION FUNCTIONS //
void animateIdle(){
	glutPostRedisplay();
	Sleep(ANIMATION_DELAY);
}
// Kings a piece that is at the selector
void animateKingPiece(){
	animationTime += ANIMATION_INCREMENT * MOVING_ANIMATION_SPEEDUP;
	movingY = pow(1 - animationTime, 2) * CAMERA_HEIGHT;
	postAnimation(postAnimateKingPiece);
}

// Moves selected piece from current position to desired position
float movingXIncrement, movingZIncrement;
void animateMovePiece(){
	animationTime += ANIMATION_INCREMENT * MOVING_ANIMATION_SPEEDUP;
	movingX += movingXIncrement;
	movingY = sin(animationTime * PI) * MOVING_PIECE_HEIGHT;
	movingZ += movingZIncrement;
	postAnimation(postAnimateMovePiece);
}

// Removes a piece from the board
void animateRemovePiece(){
	animationTime += ANIMATION_INCREMENT * MOVING_ANIMATION_SPEEDUP;
	movingY = pow(animationTime, 2) * CAMERA_HEIGHT;
	postAnimation(postAnimateRemovePiece);
}

// Rotates camera to next player side
void animateRotateCamera(){
	animationTime += ANIMATION_INCREMENT;
	cameraAngle = ((-cos(PI * animationTime) + 1) / 2) * PI;
	if(!isPlayerOne){
		cameraAngle += PI;
	}
	postAnimation(postAnimateRotateCamera);
}
///////////////////////////////////


// ANIMATION CALLBACKS (RUN ONLY BY ANIMATIONS!) //
// Finalize kinging piece callback
void postAnimateKingPiece(){
	isKingingPiece = false;
	gameBoard[selectY][selectX] = gameBoard[selectY][selectX] | 0x4;
	finishMove();
}

// Finalize moving piece callback
void postAnimateMovePiece(){
	isMovingPiece = false;
	gameBoard[selectY][selectX] = gameBoard[movingPiecePos[0]][movingPiecePos[1]];
	gameBoard[movingPiecePos[0]][movingPiecePos[1]] = 0;
	if((isPlayerOne ? selectY == BOARD_DIMENSION - 1 : selectY == 0) && gameBoard[selectY][selectX] < 3){
		isKingingPiece = true;
		movingX = 0.0;
		movingY = 0.0;
		movingZ = 0.0;
		setAnimation(animateKingPiece);
	}
	else{
		finishMove();
	}
}

// Finalize removing piece callback
void postAnimateRemovePiece(){
	isMovingPiece = false;
	gameBoard[movingPiecePos[0]][movingPiecePos[1]] = 0;

	continuingMove = true;
	isSelectingTarget = false;
	collectTargets();
	if(hasRemovableTargets){
		movingPiecePos[0] = selectY;
		movingPiecePos[1] = selectX;
		isSelectingTarget = true;
		canDeselect = false;
		canInteract = true;
	}
	else{
		toggleActivePlayer();
	}
}

// Finalize rotating camera callback
void postAnimateRotateCamera(){
	cameraAngle = isPlayerOne ? PI : 0.0;
}
///////////////////////////////////////////////////


// MENU CUSTOMIZER //
void (* decrementCustomizerFunc)(void);
void (* incrementCustomizerFunc)(void);
void (* postCustomizerFunc)(void);

float* floatCustomizer;
float floatCustomizerIncrement;
int* intCustomizer;
int intCustomizerIncrement;

void floatDecrementFunc(){
	(*floatCustomizer) -= floatCustomizerIncrement;
}
void floatIncrementFunc(){
	(*floatCustomizer) += floatCustomizerIncrement;
}
void intDecrementFunc(){
	(*intCustomizer) -= intCustomizerIncrement;
}
void intIncrementFunc(){
	(*intCustomizer) += intCustomizerIncrement;
}

void decrementCustomizer(){
	if(decrementCustomizerFunc){
		decrementCustomizerFunc();
		if(postCustomizerFunc){
			postCustomizerFunc();
		}
	}
}

void incrementCustomizer(){
	if(incrementCustomizerFunc){
		incrementCustomizerFunc();
		if(postCustomizerFunc){
			postCustomizerFunc();
		}
	}
}
/////////////////////


// WORKLOAD //
// Collects valid target information based on selector position
void collectTargets(){
	if(!isSelectingTarget){
		hasRemovableTargets = false;
		hasTargets = false;
		for(int y = 0; y < BOARD_DIMENSION; y++){
			for(int x = 0; x < BOARD_DIMENSION; x++){
				moveToRemoves[y][x][0] = -1;
				moveToRemoves[y][x][1] = -1;
				validTargets[y][x] = false;
			}
		}
		int piece = gameBoard[selectY][selectX];
		if(piece > 0 && isActivePlayerPiece(piece)){
			int moveDirs[] = {isPlayerOne ? 1 : -1, piece > 3 ? isPlayerOne ? -1 : 1 : 0};
			int moveDir, moveX, moveY, jumpX, jumpY;
			for(int y = 0; y < 2; y++){
				moveDir = moveDirs[y];
				if(moveDir != 0){
					moveY = selectY + moveDir;
					for(int x = -1; x < 2; x += 2){
						moveX = selectX + x;
						if(isInBoard(moveY, moveX)){
							piece = gameBoard[moveY][moveX];
							if(piece == 0){
								if(!continuingMove){
									hasTargets = true;
									validTargets[moveY][moveX] = true;
								}
							}
							else if(!isActivePlayerPiece(piece)){
								jumpX = moveX + x;
								jumpY = moveY + moveDir;
								if(isInBoard(jumpY, jumpX)){
									if(gameBoard[jumpY][jumpX] == 0){
										hasRemovableTargets = true;
										hasTargets = true;
										moveToRemoves[jumpY][jumpX][0] = moveY;
										moveToRemoves[jumpY][jumpX][1] = moveX;
										validTargets[jumpY][jumpX] = true;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}

// Moves selector left or down relative to active player
bool decrementSelector(bool isY){
	if(isY){
		if(selectY - 1 >= 0){
			selectY--;
			return true;
		}
	}
	else{
		if(selectX - 1 >= 0){
			selectX--;
			return true;
		}
	}
	return false;
}

// Continues a turn or switches to next player
void finishMove(){
	if(!removePiece()){
		toggleActivePlayer();
	}
}

// Moves selector right or up relative to active player
bool incrementSelector(bool isY){
	if(isY){
		if(selectY + 1 < BOARD_DIMENSION){
			selectY++;
			return true;
		}
	}
	else{
		if(selectX + 1 < BOARD_DIMENSION){
			selectX++;
			return true;
		}
	}
	return false;
}

// Moves selector based on 'dir' (0 = up, 1 = left, 2 = down, 3 = right)
void moveSelector(int dir){
	bool pass = false;
	switch(dir){
		case 0:
			pass = isPlayerOne ? incrementSelector(true) : decrementSelector(true);
			break;
		case 1:
			pass = isPlayerOne ? decrementSelector(false) : incrementSelector(false);
			break;
		case 2:
			pass = isPlayerOne ? decrementSelector(true) : incrementSelector(true);
			break;
		case 3:
			pass = isPlayerOne ? incrementSelector(false) : decrementSelector(false);
			break;
	}
	if(pass){
		collectTargets();
	}
}

// Moves selected piece from current position to desired position (Uses animation)
void movePiece(){
	isMovingPiece = true;
	movingXIncrement = ((selectY - movingPiecePos[0]) * SQUARE_SIZE) * ANIMATION_INCREMENT * MOVING_ANIMATION_SPEEDUP;
	movingZIncrement = ((selectX - movingPiecePos[1]) * SQUARE_SIZE) * ANIMATION_INCREMENT * MOVING_ANIMATION_SPEEDUP;
	movingX = 0.0;
	movingY = 0.0;
	movingZ = 0.0;
	setAnimation(animateMovePiece);
}

// Removes a piece from the board (Uses animation)
bool removePiece(){
	int* removedPiece = moveToRemoves[selectY][selectX];
	if(removedPiece[0] >= 0){
		isMovingPiece = true;
		movingPiecePos[0] = removedPiece[0];
		movingPiecePos[1] = removedPiece[1];
		movingX = 0.0;
		movingY = 0.0;
		movingZ = 0.0;
		setAnimation(animateRemovePiece);
		return true;
	}
	return false;
}

// Initializes board and values for new game
static int curBoardDimension;
void resetBoard(){
	isPlayerOne = false;
	if(gameBoard){
		for(int y = 0; y < curBoardDimension; y++){
			delete [] gameBoard[y];
			delete [] validTargets[y];
			for(int x = 0; x < 2; x++){
				delete [] moveToRemoves[y][x];
			}
			delete [] moveToRemoves[y];
		}
		delete [] gameBoard;
		delete [] moveToRemoves;
		delete [] validTargets;
	}
	curBoardDimension = BOARD_DIMENSION;
	gameBoard = new int*[BOARD_DIMENSION];
	moveToRemoves = new int**[BOARD_DIMENSION];
	validTargets = new bool*[BOARD_DIMENSION];
	int midEnd = BOARD_DIMENSION - STARTING_ROWS - 1;
	for(int y = 0; y < BOARD_DIMENSION; y++){
		gameBoard[y] = new int[BOARD_DIMENSION];
		moveToRemoves[y] = new int*[BOARD_DIMENSION];
		validTargets[y] = new bool[BOARD_DIMENSION];
		for(int x = 0; x < BOARD_DIMENSION; x++){
			gameBoard[y][x] = y < STARTING_ROWS && (y + x) % 2 == 0 ? 1 : y > midEnd && (y + x) % 2 == 0 ? 2 : 0;
			moveToRemoves[y][x] = new int[2];
			moveToRemoves[y][x][0] = -1;
			moveToRemoves[y][x][1] = -1;
			validTargets[y][x] = false;
		}
	}
	toggleActivePlayer();
}

// Toggles currently active player (Uses animation)
void toggleActivePlayer(){
	isPlayerOne = !isPlayerOne;
	if(isPlayerOne){
		selectX = BOARD_DIMENSION / 2;
		selectY = 0;
	}
	else{
		selectX = (BOARD_DIMENSION / 2) - 1;
		selectY = BOARD_DIMENSION - 1;
	}
	canDeselect = true;
	continuingMove = false;
	isSelectingTarget = false;
	collectTargets();
	setAnimation(animateRotateCamera);
}

// Toggles special shading
void toggleSpecialShading(){
	specialShading = !specialShading;
	if(specialShading){
		glClearColor(0.0, 0.0, 0.0, 0.0);
		glEnable(GL_LIGHTING);
		glEnable(GL_LIGHT0);
	}
	else{
		glClearColor(0.75, 0.75, 0.75, 0.0);
		glDisable(GL_LIGHTING);
		glDisable(GL_LIGHT0);
	}
}
//////////////


void drawPiecePoints(int pieceColorIndex, float pieceHeight){
	glColor3fv(PIECE_COLOR[pieceColorIndex]);
	glNormal3fv(VECTOR_UP.v);
	glBegin(GL_POLYGON);
		for(int i = 0; i < PIECE_EDGES; i++){
			glVertex3f(PIECE_VERTICIES[0][i], pieceHeight, PIECE_VERTICIES[1][i]);
		}
	glEnd();
	glColor3fv(PIECE_SIDE_COLORS[pieceColorIndex]);
	glBegin(GL_QUAD_STRIP);
		for(int i = 0; i < PIECE_EDGES; i++){
			if(i + 1 == PIECE_EDGES){
				glNormal3fv(getNormal(PIECE_VERTICIES[0][i], 0.0, PIECE_VERTICIES[1][i], PIECE_VERTICIES[0][i], pieceHeight, PIECE_VERTICIES[1][i], PIECE_VERTICIES[0][0], pieceHeight, PIECE_VERTICIES[1][0]));
			}
			else{
				glNormal3fv(getNormal(PIECE_VERTICIES[0][i], 0.0, PIECE_VERTICIES[1][i], PIECE_VERTICIES[0][i], pieceHeight, PIECE_VERTICIES[1][i], PIECE_VERTICIES[0][i + 1], pieceHeight, PIECE_VERTICIES[1][i + 1]));
			}
			glVertex3f(PIECE_VERTICIES[0][i], 0.0, PIECE_VERTICIES[1][i]);
			glVertex3f(PIECE_VERTICIES[0][i], pieceHeight, PIECE_VERTICIES[1][i]);
		}
		glNormal3fv(getNormal(PIECE_VERTICIES[0][0], 0.0, PIECE_VERTICIES[1][0], PIECE_VERTICIES[0][0], pieceHeight, PIECE_VERTICIES[1][0], PIECE_VERTICIES[0][1], pieceHeight, PIECE_VERTICIES[1][1]));
		glVertex3f(PIECE_VERTICIES[0][0], 0.0, PIECE_VERTICIES[1][0]);
		glVertex3f(PIECE_VERTICIES[0][0], pieceHeight, PIECE_VERTICIES[1][0]);
	glEnd();
}

void displayFunc(){
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();

	gluLookAt(cos(cameraAngle) * CAMERA_DISTANCE, CAMERA_HEIGHT, sin(cameraAngle) * CAMERA_DISTANCE, 0.0, 0.0, 0.0, 0.0, 1.0, 0.0);

	float shadowColor[4] = {SHADOW_COLOR_INTENSITY, SHADOW_COLOR_INTENSITY, SHADOW_COLOR_INTENSITY, SHADOW_COLOR_ALPHA};
	glLightfv(GL_LIGHT0, GL_AMBIENT, shadowColor);
	float flickerHalfRange = FLICKER_RANGE / 2.0;
	float lightColor[4] = {LIGHT_COLOR[0], lightFlicker ? LIGHT_COLOR[1] + (((rand() % FLICKER_RANGE) - flickerHalfRange) * FLICKER_MULTIPLIER) : LIGHT_COLOR[1], LIGHT_COLOR[2], LIGHT_COLOR[3]};
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightColor);
	glLightfv(GL_LIGHT0, GL_POSITION, LIGHT_POSITION);
	glMaterialfv(GL_FRONT, GL_EMISSION, DEFAULT_COLOR);

	glPushMatrix();
	glTranslatef(-SQUARE_SIZE * (BOARD_DIMENSION / 2), 0.0, -SQUARE_SIZE * (BOARD_DIMENSION / 2));

	// Game board
	glPushMatrix();
	glColor3fv(EDGE_SIDE_COLOR);
	glNormal3fv(VECTOR_NORTH.v);
	glBegin(GL_QUADS);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(0.0, EDGE_HEIGHT, 0.0);
		glVertex3f(EDGE_INNER_LENGTH, EDGE_HEIGHT, 0.0);
		glVertex3f(EDGE_INNER_LENGTH, 0.0, 0.0);
	glEnd();
	glNormal3fv(VECTOR_WEST.v);
	glBegin(GL_QUADS);
		glVertex3f(EDGE_INNER_LENGTH, 0.0, 0.0);
		glVertex3f(EDGE_INNER_LENGTH, EDGE_HEIGHT, 0.0);
		glVertex3f(EDGE_INNER_LENGTH, EDGE_HEIGHT, EDGE_INNER_LENGTH);
		glVertex3f(EDGE_INNER_LENGTH, 0.0, EDGE_INNER_LENGTH);
	glEnd();
	glNormal3fv(VECTOR_SOUTH.v);
	glBegin(GL_QUADS);
		glVertex3f(EDGE_INNER_LENGTH, 0.0, EDGE_INNER_LENGTH);
		glVertex3f(EDGE_INNER_LENGTH, EDGE_HEIGHT, EDGE_INNER_LENGTH);
		glVertex3f(0.0, EDGE_HEIGHT, EDGE_INNER_LENGTH);
		glVertex3f(0.0, 0.0, EDGE_INNER_LENGTH);
	glEnd();
	glNormal3fv(VECTOR_EAST.v);
	glBegin(GL_QUADS);
		glVertex3f(0.0, 0.0, EDGE_INNER_LENGTH);
		glVertex3f(0.0, EDGE_HEIGHT, EDGE_INNER_LENGTH);
		glVertex3f(0.0, EDGE_HEIGHT, 0.0);
		glVertex3f(0.0, 0.0, 0.0);
	glEnd();

	glTranslatef(0.0, EDGE_HEIGHT, 0.0);
	glColor3fv(PIECE_SIDE_COLORS[0]);
	glNormal3fv(VECTOR_UP.v);
	glBegin(GL_QUADS);
		glVertex3f(0.0, 0.0, EDGE_INNER_LENGTH);
		glVertex3f(-EDGE_THICKNESS, 0.0, EDGE_JUMP_LENGTH);
		glVertex3f(-EDGE_THICKNESS, 0.0, -EDGE_THICKNESS);
		glVertex3f(0.0, 0.0, 0.0);
	glEnd();
	glColor3fv(PIECE_SIDE_COLORS[1]);
	glBegin(GL_QUADS);
		glVertex3f(EDGE_INNER_LENGTH, 0.0, 0.0);
		glVertex3f(EDGE_JUMP_LENGTH, 0.0, -EDGE_THICKNESS);
		glVertex3f(EDGE_JUMP_LENGTH, 0.0, EDGE_JUMP_LENGTH);
		glVertex3f(EDGE_INNER_LENGTH, 0.0, EDGE_INNER_LENGTH);
	glEnd();
	glColor3fv(EDGE_COLOR);
	glBegin(GL_QUADS);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(-EDGE_THICKNESS, 0.0, -EDGE_THICKNESS);
		glVertex3f(EDGE_JUMP_LENGTH, 0.0, -EDGE_THICKNESS);
		glVertex3f(EDGE_INNER_LENGTH, 0.0, 0.0);
	glEnd();
	glBegin(GL_QUADS);
		glVertex3f(EDGE_INNER_LENGTH, 0.0, EDGE_INNER_LENGTH);
		glVertex3f(EDGE_JUMP_LENGTH, 0.0, EDGE_JUMP_LENGTH);
		glVertex3f(-EDGE_THICKNESS, 0.0, EDGE_JUMP_LENGTH);
		glVertex3f(0.0, 0.0, EDGE_INNER_LENGTH);
	glEnd();

	glTranslatef(-EDGE_THICKNESS, -EDGE_OUTER_HEIGHT, -EDGE_THICKNESS);
	glColor3fv(EDGE_SIDE_COLOR);
	glNormal3fv(VECTOR_WEST.v);
	glBegin(GL_QUADS);
		glVertex3f(0.0, 0.0, 0.0);
		glVertex3f(0.0, EDGE_OUTER_HEIGHT, 0.0);
		glVertex3f(0.0, EDGE_OUTER_HEIGHT, EDGE_OUTER_LENGTH);
		glVertex3f(0.0, 0.0, EDGE_OUTER_LENGTH);
	glEnd();
	glNormal3fv(VECTOR_NORTH.v);
	glBegin(GL_QUADS);
		glVertex3f(0.0, 0.0, EDGE_OUTER_LENGTH);
		glVertex3f(0.0, EDGE_OUTER_HEIGHT, EDGE_OUTER_LENGTH);
		glVertex3f(EDGE_OUTER_LENGTH, EDGE_OUTER_HEIGHT, EDGE_OUTER_LENGTH);
		glVertex3f(EDGE_OUTER_LENGTH, 0.0, EDGE_OUTER_LENGTH);
	glEnd();
	glNormal3fv(VECTOR_EAST.v);
	glBegin(GL_QUADS);
		glVertex3f(EDGE_OUTER_LENGTH, 0.0, EDGE_OUTER_LENGTH);
		glVertex3f(EDGE_OUTER_LENGTH, EDGE_OUTER_HEIGHT, EDGE_OUTER_LENGTH);
		glVertex3f(EDGE_OUTER_LENGTH, EDGE_OUTER_HEIGHT, 0.0);
		glVertex3f(EDGE_OUTER_LENGTH, 0.0, 0.0);
	glEnd();
	glNormal3fv(VECTOR_SOUTH.v);
	glBegin(GL_QUADS);
		glVertex3f(EDGE_OUTER_LENGTH, 0.0, 0.0);
		glVertex3f(EDGE_OUTER_LENGTH, EDGE_OUTER_HEIGHT, 0.0);
		glVertex3f(0.0, EDGE_OUTER_HEIGHT, 0.0);
		glVertex3f(0.0, 0.0, 0.0);
	glEnd();
	glPopMatrix();

	// Grid items
	int pieceData, pieceColorIndex;
	float pieceHeight;
	bool isKing;
	float* specialSquareColor;
	float halfSquareSize = SQUARE_SIZE / 2;
	for(int y = 0; y < BOARD_DIMENSION; y++){
		if(y > 0){
			glTranslatef(SQUARE_SIZE, 0.0, 0.0);
		}
		glPushMatrix();
		for(int x = 0; x < BOARD_DIMENSION; x++){
			if(x > 0){
				glTranslatef(0.0, 0.0, SQUARE_SIZE);
			}
			glColor3fv(SQUARE_COLOR[(y + x) % 2]);
			glMaterialfv(GL_FRONT, GL_EMISSION, DEFAULT_COLOR);
			glNormal3fv(VECTOR_UP.v);
			glBegin(GL_POLYGON);
				glVertex3f(0.0, 0.0, 0.0);
				glVertex3f(SQUARE_SIZE, 0.0, 0.0);
				glVertex3f(SQUARE_SIZE, 0.0, SQUARE_SIZE);
				glVertex3f(0.0, 0.0, SQUARE_SIZE);
			glEnd();

			pieceData = gameBoard[y][x];
			if(pieceData > 0){
				pieceColorIndex = (pieceData & 0x3) - 1;
				isKing = pieceData > 3;
				pieceHeight = isKing ? 2 * PIECE_HEIGHT : PIECE_HEIGHT;

				glPushMatrix();
				glTranslatef(halfSquareSize, 0.0, halfSquareSize);
				if(isMovingPiece && movingPiecePos[0] == y && movingPiecePos[1] == x){
					glTranslatef(movingX, movingY, movingZ);
				}
				drawPiecePoints(pieceColorIndex, pieceHeight);
				if(isKingingPiece && selectY == y && selectX == x){
					glTranslatef(movingX, pieceHeight + movingY, movingZ);
					drawPiecePoints(pieceColorIndex, pieceHeight);
				}
				glPopMatrix();
			}

			if(canInteract){
				if(isSelectingTarget){
					specialSquareColor = y == selectY && x == selectX ? validTargets[y][x] ? SELECTOR_COLOR[1] : SELECTOR_COLOR[0] : validTargets[y][x] ? TARGET_COLOR : NULL;
				}
				else{
					specialSquareColor = y == selectY && x == selectX ? pieceData > 0 ? hasTargets ? SELECTOR_COLOR[1] : SELECTOR_COLOR[2] : SELECTOR_COLOR[0] : hasTargets && validTargets[y][x] ? POSSIBLE_TARGET_COLOR : NULL;
				}
				if(specialSquareColor){
					glColor4fv(specialSquareColor);
					glMaterialfv(GL_FRONT, GL_EMISSION, specialSquareColor);
					glNormal3fv(VECTOR_UP.v);
					glBegin(GL_POLYGON);
						glVertex3f(0.0, SELECTOR_HEIGHT_OFFSET, 0.0);
						glVertex3f(SQUARE_SIZE, SELECTOR_HEIGHT_OFFSET, 0.0);
						glVertex3f(SQUARE_SIZE, SELECTOR_HEIGHT_OFFSET, SQUARE_SIZE);
						glVertex3f(0.0, SELECTOR_HEIGHT_OFFSET, SQUARE_SIZE);
					glEnd();
				}
			}
		}
		glPopMatrix();
	}
	glPopMatrix();


	if(specialShading){
		glTranslatef(0.0, SHADOW_HEIGHT_OFFSET, 0.0);
		glMaterialfv(GL_FRONT, GL_EMISSION, DEFAULT_COLOR);
		glColor4fv(shadowColor);
		glNormal3fv(VECTOR_UP.v);
		float u;
		for(int y = 0; y < BOARD_DIMENSION; y++){
			for(int x = 0; x < BOARD_DIMENSION; x++){
				if(gameBoard[y][x] > 0 && !(isMovingPiece && movingPiecePos[0] == y && movingPiecePos[1] == x)){
					glPushMatrix();
					u = -LIGHT_POSITION[1] / ((gameBoard[y][x] > 3 ? PIECE_HEIGHT * 2.0 : PIECE_HEIGHT) - LIGHT_POSITION[1]);
					glTranslatef(LIGHT_POSITION[0] + (u * ((((y - (BOARD_DIMENSION / 2)) * SQUARE_SIZE) + (SQUARE_SIZE / 2.0)) - LIGHT_POSITION[0])), 0.0, LIGHT_POSITION[2] + (u * ((((x - (BOARD_DIMENSION / 2)) * SQUARE_SIZE) + (SQUARE_SIZE / 2.0)) - LIGHT_POSITION[2])));
					if(shadowFlicker){
						glTranslatef(((rand() % FLICKER_RANGE) - flickerHalfRange) * FLICKER_MULTIPLIER, 0.0, ((rand() % FLICKER_RANGE) - flickerHalfRange) * FLICKER_MULTIPLIER);
					}
					glBegin(GL_POLYGON);
						for(int i = 0; i < PIECE_EDGES; i++){
							glVertex3f(PIECE_VERTICIES[0][i] * SHADOW_SIZE_MULTIPLIER, 0.0, PIECE_VERTICIES[1][i] * SHADOW_SIZE_MULTIPLIER);
						}
					glEnd();
					glPopMatrix();
				}
			}
		}
	}

	glFlush();
	glutSwapBuffers();
}

void resizeFunc(int w, int h){
	glViewport(0, 0, (GLsizei) w, (GLsizei) h);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluPerspective(45.0, (GLfloat) w / (GLfloat) h, 1.0, 500.0);
	glMatrixMode(GL_MODELVIEW);
}

void initWindow(){
	glutInitDisplayMode(GLUT_DEPTH | GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowPosition(10, 10);
	glutInitWindowSize(800, 600);
	glutCreateWindow("Final Project: Checkers");

	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glEnable(GL_COLOR_MATERIAL);
	glColorMaterial(GL_FRONT_AND_BACK, GL_AMBIENT_AND_DIFFUSE);

	glEnable(GL_DEPTH_TEST);

	toggleSpecialShading();
	glShadeModel(GL_SMOOTH);

	glutDisplayFunc(displayFunc);
	glutReshapeFunc(resizeFunc);
}


void keyboardFunc(unsigned char key, int x, int y){
	if(canInteract){
		switch(key){
			case 'w':
			case 'W':
				moveSelector(0);
				break;
			case 'a':
			case 'A':
				moveSelector(1);
				break;
			case 's':
			case 'S':
				moveSelector(2);
				break;
			case 'd':
			case 'D':
				moveSelector(3);
				break;
			case ' ':  //  Confirm Selection  (space and enter)
			case 13:
				if(isSelectingTarget){
					if(validTargets[selectY][selectX]){
						movePiece();
					}
					else if(canDeselect){
						isSelectingTarget = false;
						collectTargets();
					}
				}
				else{
					if(hasTargets){
						isSelectingTarget = true;
						movingPiecePos[0] = selectY;
						movingPiecePos[1] = selectX;
					}
				}
				break;
			case 27:  //  De-select Selection  (escape)
				if(isSelectingTarget && canDeselect){
					isSelectingTarget = false;
					collectTargets();
				}
				break;
			case 'v':
			case 'V':
				toggleSpecialShading();
				break;
			case '-':
			case '_':
				lightFlicker = !lightFlicker;
				break;
			case '=':
			case '+':
				shadowFlicker = !shadowFlicker;
				break;
			case '\\':
				doIdleFunction = !doIdleFunction;
				glutIdleFunc(doIdleFunction ? animateIdle : NULL);
				break;
			case ',':
			case '<':
				decrementCustomizer();
				break;
			case '.':
			case '>':
				incrementCustomizer();
				break;
		}
		glutPostRedisplay();
	}
}

void specialFunc(int key, int x, int y){
	if(canInteract){
		switch(key){
			case GLUT_KEY_UP:
				moveSelector(0);
				break;
			case GLUT_KEY_LEFT:
				moveSelector(1);
				break;
			case GLUT_KEY_DOWN:
				moveSelector(2);
				break;
			case GLUT_KEY_RIGHT:
				moveSelector(3);
				break;
		}
		glutPostRedisplay();
	}
}

void initActions(){
	glutKeyboardFunc(keyboardFunc);
	glutSpecialFunc(specialFunc);
}


void calculateEdgeResetFunc(){
	calculateEdgeLengths();
	resetBoard();
}
void menuFunc(int val){
	floatCustomizer = NULL;
	floatCustomizerIncrement = 0.01;
	intCustomizer = NULL;
	intCustomizerIncrement = 1;
	postCustomizerFunc = NULL;
	switch(val){
		case MENU_CAMERA_DISTANCE:
			floatCustomizer = &CAMERA_DISTANCE;
			floatCustomizerIncrement = 1.0;
			break;
		case MENU_CAMERA_HEIGHT:
			floatCustomizer = &CAMERA_HEIGHT;
			floatCustomizerIncrement = 1.0;
			break;
		case MENU_COLOR_PIECE_1_R:
		case MENU_COLOR_PIECE_1_G:
		case MENU_COLOR_PIECE_1_B:
			floatCustomizer = &PIECE_COLOR[0][val - MENU_COLOR_PIECE_1_R];
			postCustomizerFunc = calculateSideColors;
			break;
		case MENU_COLOR_PIECE_2_R:
		case MENU_COLOR_PIECE_2_G:
		case MENU_COLOR_PIECE_2_B:
			floatCustomizer = &PIECE_COLOR[1][val - MENU_COLOR_PIECE_2_R];
			postCustomizerFunc = calculateSideColors;
			break;
		case MENU_COLOR_SQUARE_1_R:
		case MENU_COLOR_SQUARE_1_G:
		case MENU_COLOR_SQUARE_1_B:
			floatCustomizer = &SQUARE_COLOR[0][val - MENU_COLOR_SQUARE_1_R];
			postCustomizerFunc = calculateSideColors;
			break;
		case MENU_COLOR_SQUARE_2_R:
		case MENU_COLOR_SQUARE_2_G:
		case MENU_COLOR_SQUARE_2_B:
			floatCustomizer = &SQUARE_COLOR[1][val - MENU_COLOR_SQUARE_2_R];
			postCustomizerFunc = calculateSideColors;
			break;
		case MENU_COLOR_EDGE_R:
		case MENU_COLOR_EDGE_G:
		case MENU_COLOR_EDGE_B:
			floatCustomizer = &EDGE_COLOR[val - MENU_COLOR_EDGE_R];
			postCustomizerFunc = calculateSideColors;
			break;
		case MENU_COLOR_SELECTOR_1_R:
		case MENU_COLOR_SELECTOR_1_G:
		case MENU_COLOR_SELECTOR_1_B:
		case MENU_COLOR_SELECTOR_1_A:
			floatCustomizer = &SELECTOR_COLOR[0][val - MENU_COLOR_SELECTOR_1_R];
			break;
		case MENU_COLOR_SELECTOR_2_R:
		case MENU_COLOR_SELECTOR_2_G:
		case MENU_COLOR_SELECTOR_2_B:
		case MENU_COLOR_SELECTOR_2_A:
			floatCustomizer = &SELECTOR_COLOR[1][val - MENU_COLOR_SELECTOR_2_R];
			break;
		case MENU_COLOR_SELECTOR_3_R:
		case MENU_COLOR_SELECTOR_3_G:
		case MENU_COLOR_SELECTOR_3_B:
		case MENU_COLOR_SELECTOR_3_A:
			floatCustomizer = &SELECTOR_COLOR[2][val - MENU_COLOR_SELECTOR_3_R];
			break;
		case MENU_COLOR_POSSIBLE_TARGET_R:
		case MENU_COLOR_POSSIBLE_TARGET_G:
		case MENU_COLOR_POSSIBLE_TARGET_B:
		case MENU_COLOR_POSSIBLE_TARGET_A:
			floatCustomizer = &POSSIBLE_TARGET_COLOR[val - MENU_COLOR_POSSIBLE_TARGET_R];
			break;
		case MENU_COLOR_TARGET_R:
		case MENU_COLOR_TARGET_G:
		case MENU_COLOR_TARGET_B:
		case MENU_COLOR_TARGET_A:
			floatCustomizer = &TARGET_COLOR[val - MENU_COLOR_TARGET_R];
			break;
		case MENU_COLOR_LIGHT_R:
		case MENU_COLOR_LIGHT_G:
		case MENU_COLOR_LIGHT_B:
			floatCustomizer = &LIGHT_COLOR[val - MENU_COLOR_LIGHT_R];
			break;
		case MENU_COLOR_SHADOW_I:
			floatCustomizer = &SHADOW_COLOR_INTENSITY;
			break;
		case MENU_COLOR_SHADOW_A:
			floatCustomizer = &SHADOW_COLOR_ALPHA;
			break;
		case MENU_COLOR_DARKEN:
			floatCustomizer = &DARKEN_OFFSET;
			postCustomizerFunc = calculateSideColors;
			break;
		case MENU_GAME_BOARD_DIMENSIONS:
			intCustomizer = &BOARD_DIMENSION;
			postCustomizerFunc = calculateEdgeResetFunc;
			break;
		case MENU_GAME_STARTING_ROWS:
			intCustomizer = &STARTING_ROWS;
			postCustomizerFunc = resetBoard;
			break;
		case MENU_VISUAL_PIECE_EDGES:
			intCustomizer = &PIECE_EDGES;
			postCustomizerFunc = calculatePieceVerticies;
			break;
		case MENU_VISUAL_PIECE_HEIGHT:
			floatCustomizer = &PIECE_HEIGHT;
			floatCustomizerIncrement = 0.1;
			break;
		case MENU_VISUAL_PIECE_RADIUS:
			floatCustomizer = &PIECE_RADIUS;
			postCustomizerFunc = calculatePieceVerticies;
			break;
		case MENU_VISUAL_EDGE_HEIGHT:
			floatCustomizer = &EDGE_HEIGHT;
			floatCustomizerIncrement = 0.1;
			postCustomizerFunc = calculateEdgeLengths;
			break;
		case MENU_VISUAL_EDGE_THICKNESS:
			floatCustomizer = &EDGE_THICKNESS;
			floatCustomizerIncrement = 0.1;
			postCustomizerFunc = calculateEdgeLengths;
			break;
		case MENU_VISUAL_SQUARE_SIZE:
			floatCustomizer = &SQUARE_SIZE;
			postCustomizerFunc = calculateEdgeLengths;
			break;
		case MENU_VISUAL_SELECTOR_HEIGHT:
			floatCustomizer = &SELECTOR_HEIGHT_OFFSET;
			floatCustomizerIncrement = 0.1;
			break;
		case MENU_VISUAL_LIGHT_POSITION_X:
		case MENU_VISUAL_LIGHT_POSITION_Y:
		case MENU_VISUAL_LIGHT_POSITION_Z:
			floatCustomizer = &LIGHT_POSITION[val - MENU_VISUAL_LIGHT_POSITION_X];
			floatCustomizerIncrement = 1.0;
			break;
		case MENU_VISUAL_SHADOW_HEIGHT:
			floatCustomizer = &SHADOW_HEIGHT_OFFSET;
			floatCustomizerIncrement = 0.1;
			break;
		case MENU_VISUAL_SHADOW_MULTIPLIER:
			floatCustomizer = &SHADOW_SIZE_MULTIPLIER;
			break;
		case MENU_ANIMATION_FLICKER_RANGE:
			intCustomizer = &FLICKER_RANGE;
			break;
		case MENU_ANIMATION_FLICKER_MULTIPLIER:
			floatCustomizer = &FLICKER_MULTIPLIER;
			floatCustomizerIncrement = 0.0001;
			break;
		case MENU_ANIMATION_DELAY:
			intCustomizer = &ANIMATION_DELAY;
			break;
		case MENU_ANIMATION_INCREMENT:
			floatCustomizer = &ANIMATION_INCREMENT;
			floatCustomizerIncrement = 0.001;
			break;
		case MENU_ANIMATION_MOVING_SPEEDUP:
			floatCustomizer = &MOVING_ANIMATION_SPEEDUP;
			floatCustomizerIncrement = 0.1;
			break;
		case MENU_ANIMATION_MOVING_HEIGHT:
			floatCustomizer = &MOVING_PIECE_HEIGHT;
			floatCustomizerIncrement = 0.1;
			break;
		case -1:
			resetBoard();
			break;
	}
	if(floatCustomizer){
		decrementCustomizerFunc = floatDecrementFunc;
		incrementCustomizerFunc = floatIncrementFunc;
	}
	else if(intCustomizer){
		decrementCustomizerFunc = intDecrementFunc;
		incrementCustomizerFunc = intIncrementFunc;
	}
}

void initMenu(){
		int menuCamera = glutCreateMenu(menuFunc);
		glutAddMenuEntry("Distance", MENU_CAMERA_DISTANCE);
		glutAddMenuEntry("Height", MENU_CAMERA_HEIGHT);

				int menuColorPiece1 = glutCreateMenu(menuFunc);
				glutAddMenuEntry("Red", MENU_COLOR_PIECE_1_R);
				glutAddMenuEntry("Green", MENU_COLOR_PIECE_1_G);
				glutAddMenuEntry("Blue", MENU_COLOR_PIECE_1_B);

				int menuColorPiece2 = glutCreateMenu(menuFunc);
				glutAddMenuEntry("Red", MENU_COLOR_PIECE_2_R);
				glutAddMenuEntry("Green", MENU_COLOR_PIECE_2_G);
				glutAddMenuEntry("Blue", MENU_COLOR_PIECE_2_B);

			int menuColorPiece = glutCreateMenu(menuFunc);
			glutAddSubMenu("Player 1", menuColorPiece1);
			glutAddSubMenu("Player 2", menuColorPiece2);

				int menuColorSquareLeft = glutCreateMenu(menuFunc);
				glutAddMenuEntry("Red", MENU_COLOR_SQUARE_1_R);
				glutAddMenuEntry("Green", MENU_COLOR_SQUARE_1_G);
				glutAddMenuEntry("Blue", MENU_COLOR_SQUARE_1_B);

				int menuColorSquareRight = glutCreateMenu(menuFunc);
				glutAddMenuEntry("Red", MENU_COLOR_SQUARE_2_R);
				glutAddMenuEntry("Green", MENU_COLOR_SQUARE_2_G);
				glutAddMenuEntry("Blue", MENU_COLOR_SQUARE_2_B);

			int menuColorSquare = glutCreateMenu(menuFunc);
			glutAddSubMenu("Left", menuColorSquareLeft);
			glutAddSubMenu("Right", menuColorSquareRight);

			int menuColorEdge = glutCreateMenu(menuFunc);
			glutAddMenuEntry("Red", MENU_COLOR_EDGE_R);
			glutAddMenuEntry("Green", MENU_COLOR_EDGE_G);
			glutAddMenuEntry("Blue", MENU_COLOR_EDGE_B);

				int menuColorSelectorNormal = glutCreateMenu(menuFunc);
				glutAddMenuEntry("Red", MENU_COLOR_SELECTOR_1_R);
				glutAddMenuEntry("Green", MENU_COLOR_SELECTOR_1_G);
				glutAddMenuEntry("Blue", MENU_COLOR_SELECTOR_1_B);
				glutAddMenuEntry("Alpha", MENU_COLOR_SELECTOR_1_A);

				int menuColorSelectorValid = glutCreateMenu(menuFunc);
				glutAddMenuEntry("Red", MENU_COLOR_SELECTOR_2_R);
				glutAddMenuEntry("Green", MENU_COLOR_SELECTOR_2_G);
				glutAddMenuEntry("Blue", MENU_COLOR_SELECTOR_2_B);
				glutAddMenuEntry("Alpha", MENU_COLOR_SELECTOR_2_A);

				int menuColorSelectorInvalid = glutCreateMenu(menuFunc);
				glutAddMenuEntry("Red", MENU_COLOR_SELECTOR_3_R);
				glutAddMenuEntry("Green", MENU_COLOR_SELECTOR_3_G);
				glutAddMenuEntry("Blue", MENU_COLOR_SELECTOR_3_B);
				glutAddMenuEntry("Alpha", MENU_COLOR_SELECTOR_3_A);

			int menuColorSelector = glutCreateMenu(menuFunc);
			glutAddSubMenu("Normal", menuColorSelectorNormal);
			glutAddSubMenu("Valid", menuColorSelectorValid);
			glutAddSubMenu("Invalid", menuColorSelectorInvalid);

			int menuColorPossibleTarget = glutCreateMenu(menuFunc);
			glutAddMenuEntry("Red", MENU_COLOR_POSSIBLE_TARGET_R);
			glutAddMenuEntry("Green", MENU_COLOR_POSSIBLE_TARGET_G);
			glutAddMenuEntry("Blue", MENU_COLOR_POSSIBLE_TARGET_B);
			glutAddMenuEntry("Alpha", MENU_COLOR_POSSIBLE_TARGET_A);

			int menuColorTarget = glutCreateMenu(menuFunc);
			glutAddMenuEntry("Red", MENU_COLOR_TARGET_R);
			glutAddMenuEntry("Green", MENU_COLOR_TARGET_G);
			glutAddMenuEntry("Blue", MENU_COLOR_TARGET_B);
			glutAddMenuEntry("Alpha", MENU_COLOR_TARGET_A);

			int menuColorLight = glutCreateMenu(menuFunc);
			glutAddMenuEntry("Red", MENU_COLOR_LIGHT_R);
			glutAddMenuEntry("Green", MENU_COLOR_LIGHT_G);
			glutAddMenuEntry("Blue", MENU_COLOR_LIGHT_B);

			int menuColorShadow = glutCreateMenu(menuFunc);
			glutAddMenuEntry("Intensity", MENU_COLOR_SHADOW_I);
			glutAddMenuEntry("Alpha", MENU_COLOR_SHADOW_A);

		int menuColor = glutCreateMenu(menuFunc);
		glutAddSubMenu("Pieces", menuColorPiece);
		glutAddSubMenu("Squares", menuColorSquare);
		glutAddSubMenu("Edge", menuColorEdge);
		glutAddSubMenu("Selector", menuColorSelector);
		glutAddSubMenu("Possible Target", menuColorPossibleTarget);
		glutAddSubMenu("Target", menuColorTarget);
		glutAddSubMenu("Light", menuColorLight);
		glutAddSubMenu("Shadow", menuColorShadow);
		glutAddMenuEntry("Side Darken", MENU_COLOR_DARKEN);

		int menuGameplay = glutCreateMenu(menuFunc);
		glutAddMenuEntry("Board Dimensions", MENU_GAME_BOARD_DIMENSIONS);
		glutAddMenuEntry("Starting Rows", MENU_GAME_STARTING_ROWS);

			int menuVisualPieces = glutCreateMenu(menuFunc);
			glutAddMenuEntry("Edges", MENU_VISUAL_PIECE_EDGES);
			glutAddMenuEntry("Height", MENU_VISUAL_PIECE_HEIGHT);
			glutAddMenuEntry("Radius", MENU_VISUAL_PIECE_RADIUS);

			int menuVisualEdge = glutCreateMenu(menuFunc);
			glutAddMenuEntry("Height", MENU_VISUAL_EDGE_HEIGHT);
			glutAddMenuEntry("Thickness", MENU_VISUAL_EDGE_THICKNESS);

			int menuVisualLight = glutCreateMenu(menuFunc);
			glutAddMenuEntry("X", MENU_VISUAL_LIGHT_POSITION_X);
			glutAddMenuEntry("Y", MENU_VISUAL_LIGHT_POSITION_Y);
			glutAddMenuEntry("Z", MENU_VISUAL_LIGHT_POSITION_Z);

			int menuVisualShadow = glutCreateMenu(menuFunc);
			glutAddMenuEntry("Height", MENU_VISUAL_SHADOW_HEIGHT);
			glutAddMenuEntry("Multiplier", MENU_VISUAL_SHADOW_MULTIPLIER);

		int menuVisual = glutCreateMenu(menuFunc);
		glutAddSubMenu("Pieces", menuVisualPieces);
		glutAddSubMenu("Edge", menuVisualEdge);
		glutAddSubMenu("Light", menuVisualLight);
		glutAddSubMenu("Shadow", menuVisualShadow);
		glutAddMenuEntry("Square Size", MENU_VISUAL_SQUARE_SIZE);
		glutAddMenuEntry("Selector Height", MENU_VISUAL_SELECTOR_HEIGHT);

			int menuAnimationFlicker = glutCreateMenu(menuFunc);
			glutAddMenuEntry("Range", MENU_ANIMATION_FLICKER_RANGE);
			glutAddMenuEntry("Multiplier", MENU_ANIMATION_FLICKER_MULTIPLIER);

		int menuAnimation = glutCreateMenu(menuFunc);
		glutAddSubMenu("Flicker", menuAnimationFlicker);
		glutAddMenuEntry("Delay", MENU_ANIMATION_DELAY);
		glutAddMenuEntry("Increment", MENU_ANIMATION_INCREMENT);
		glutAddMenuEntry("Moving Speedup", MENU_ANIMATION_MOVING_SPEEDUP);
		glutAddMenuEntry("Move Height", MENU_ANIMATION_MOVING_HEIGHT);

	glutCreateMenu(menuFunc);
	glutAddMenuEntry("RESET", -1);
	glutAddSubMenu("Camera", menuCamera);
	glutAddSubMenu("Color", menuColor);
	glutAddSubMenu("Gameplay", menuGameplay);
	glutAddSubMenu("Visual", menuVisual);
	glutAddSubMenu("Animation", menuAnimation);

	glutAttachMenu(GLUT_RIGHT_BUTTON);
}


int main(int argc, char* argv[]){
	calculateEdgeLengths();
	calculatePieceVerticies();
	calculateSideColors();

	glutInit(&argc, argv);
	initWindow();
	initActions();
	initMenu();

	resetBoard();

	glutMainLoop();
	return 0;
}
